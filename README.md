# Controveries in Wikidata

Please check the full documentation for this project here: 
https://meta.wikimedia.org/wiki/Research:Identifying_Controversial_Content_in_Wikidata

## Introduction

As part of our efforts on improving knowledge integrity, we are actively working to support the community and affiliates groups to have better understanding of their projects, and also in providing tools to address knowledge integrity issues.

In this project we are aiming to support the Wikidata community, creating a framework that allows to identify content within that project that could be controversial. Identifying such content will help the community to early react to potential conflicts, and also to guide newcomers, showing them the different mechanisms that Wikidata has to manage this kind of situations. 

## Methods

In order to identify content that require special attention from admins and the community in general we operationalize the idea of “controversial content” testing different definitions, more specifically we study:

* Claims containing the “statement disputed by” qualifier.
* Content affected by “edit wars” or with multiple reverts.
* Items with large discussion on their talk pages.

Considering these definitions we perform a data analysis, to get insights about these different scopes, as well as test ML-approaches to explore the predictability of such situations.

Additionally, we run an analysis on edit spikes, to understand which are the most edited Items on Wikidata at a given point in time, and then perform a quantitative and qualitative analysis to understand the reasons behind those spikes. 

# Results

Found the updated results in this page: 
https://meta.wikimedia.org/wiki/Research:Identifying_Controversial_Content_in_Wikidata
